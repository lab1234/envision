import React, { Component } from 'react'

export default class LeftSidebar extends Component {
    render() {
        return (
            <div>
                <div class="left-sidebar">
                    <button class="control-button select-zone" disabled="disabled">Zone 1 <span class="icon-chevron-down"></span></button>
                    <h5 class="sidebar-title"><span class="icon-map-marker"></span> Zone 1</h5>
                    <div class="envision-sidebar-card">
                        <div class="card-badge">
                            <span class="icon-wifi"></span>
                        </div>
                        <div class="card-text">
                            <h5>PIR Sensor</h5>
                            <h6>MOTION UNDETECTED</h6>
                            <p class="secondary-text"><span class="icon-circle"></span> Safe</p>
                        </div>
                    </div>

                    <div class="envision-sidebar-card">
                        <div class="card-badge danger-bg">
                            <span class="icon-surround-sound"></span>
                        </div>
                        <div class="card-text">
                            <h5>Dopler</h5>
                            <h6>MOTION UNDETECTED</h6>
                            <p class="secondary-text"><span class="icon-circle"></span> Safe</p>
                        </div>
                    </div>

                    <div class="envision-sidebar-card">
                        <div class="card-badge info-bg">
                            <span class="icon-surround-sound"></span>
                        </div>
                        <div class="card-text">
                            <h5>Intrution System</h5>
                            <h6>MOTION UNDETECTED</h6>
                            <p class="secondary-text"><span class="icon-circle"></span> Safe</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
