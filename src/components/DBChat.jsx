import React from 'react';
import FooterTab from './FooterTab';
import { Link } from 'react-router-dom/cjs/react-router-dom';
import Sidebar from './Sidebar';
import Navbar from './Navbar';
import avatar from "../img/profile.jpg";
import {Helmet} from 'react-helmet';
import Chats from './Chats';
import axios from 'axios';
import io from 'socket.io-client';

const ENDPOINT = "http://127.0.0.1:5000";


class DBChat extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            user_data: {},
            socket: '',
        }

        // this.sendMessage = this.sendMessage.bind(this);
    }

    componentDidMount(){
        const auth_token = window.sessionStorage.getItem("auth_token");
        if(auth_token){
            // When users navigate to /dashboard/hub route by refreshing page
            // Check if the existing token is still valid
            
            axios.post("/check-auth", {auth_token}).then(res=>{
                if(res.headers.auth_token){

                    const user_data = res.data.user_data[0];

                    this.setState({isAuthenticated: true});
                    this.setState({user_data: user_data});
                    
                    this.setState({socket: io(ENDPOINT)})
                    
                }else{
                    this.props.history.push("/login");
                }
            });
        }else{
            this.props.history.push("/login");
        }
    }


    hideSidebar() {
        document.querySelector('.sidebar').classList.remove('slide-sidebar-right');
        document.querySelector('.transparent-overlay').classList.remove('show')
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>InternsHub - Dashboard::Home</title>
                </Helmet>
                <Sidebar />
                <div className="main">
                    <div className="transparent-overlay" onClick={() => {
                        this.hideSidebar();
                    }}></div>
                    {/* Main Content */}
                    <Navbar icon1="menu" icon2={avatar} icon3="menu-down" />
                    <div className="dropdown">
                        <Link to="" className="dropdown-link"><span className="icon-account"></span> My Profile</Link>
                        <Link to="" className="dropdown-link"><span className="icon-email"></span> Inbox</Link>
                        <Link to="" className="dropdown-link"><span className="icon-checkbox-marked-circle text-green"></span> Status</Link>
                        <hr />
                        <Link to="" className="dropdown-link"><span className="icon-logout"></span> Logout</Link>
                    </div>
                    <div className="Blog">
                    
                        {
                            (this.state.user_data.id===undefined) ? 
                            '' : 
                            <Chats user_data={this.state.user_data}/>
                        }
                            
                        
                    </div>
                    <FooterTab tabOne="active" tabTwo="none" tabThree="none" />
                </div>

            </React.Fragment>
        );
    };
}

export default DBChat;
