import React, { Component } from 'react'

export default class RightSidebar extends Component {
    constructor(props){
        super(props);

        this.snackBar = this.snackBar.bind(this)
        this.switchOn = this.switchOn.bind(this)
    }
    snackBar(msg){
        var x = document.getElementById("snackbar");
        x.innerHTML = msg;
        
        x.className = "show";
        setTimeout(()=>{ x.className = x.className.replace("show", ""); }, 3500);
    }

    switchOn(e){
        e.target.parentNode.classList.toggle('on');
        if(e.target.parentNode.classList[1]==='on'){
            e.target.parentNode.checked = true;
            e.target.parentNode.classList.add('switch-on');
            // console.log(e.target.parentNode.parentNode.firstElementChild.textContent)
            const control_sys = e.target.parentNode.parentNode.firstElementChild.textContent
            this.snackBar(`${control_sys} On`);
        }else{
            e.target.parentNode.checked = false;
            e.target.parentNode.classList.remove('switch-on');
            const control_sys = e.target.parentNode.parentNode.firstElementChild.textContent
            this.snackBar(`${control_sys} Off`);
        }
    }

    hideSidebar = async() =>{
        let overlay = await document.querySelector(".overlay-1");
        overlay.classList.remove("toggle-overlay");
        let rightbar = await document.querySelector(".right-sidebar");
        rightbar.classList.remove("showbar");
    }

    render() {
        return (
            <div className="right-sidebar-container">
                <div id="snackbar"></div>
                <div className="overlay-1" onClick={this.hideSidebar}></div>
                <div class="right-sidebar">
                    <h5 class="sidebar-title"><span className="icon-security"></span> System Controls</h5>

                    <div class="envision-sidebar-card no-pt">
                        <div class="card-badge">
                            <span class="icon-alarm"></span>
                        </div>
                        <div class="card-text">
                            <h5>Alarm Control</h5>
                            <button class="control-button" disabled="disabled">Sound Alarm &copy</button>
                            <label for="switch" class="switch">
                                <input type="checkbox" name="switch" id="switch" onChange={(e)=>{this.switchOn(e)}} />
                                <span class="control-label"></span>
                            </label>
                        </div>

                    </div>
                    <div class="envision-sidebar-card">
                        <div class="card-badge danger-bg">
                            <span class="icon-skull"></span>
                        </div>
                        <div class="card-text">
                            <h5>Protect Zone</h5>
                            <label for="switch2" class="switch">
                                <input type="checkbox" name="switch2" id="switch2" onChange={(e)=>{this.switchOn(e)}} />
                                <span class="control-label"></span>
                            </label>
                        </div>
                    </div>


                    <div class="envision-sidebar-card">
                        <div class="card-badge info-bg">
                            <span class="icon-security-network"></span>
                        </div>
                        <div class="card-text">
                            <h5>Override</h5>
                            <label for="switch3" class="switch">
                                <input type="checkbox" name="switch3" id="switch3" onChange={(e)=>{this.switchOn(e)}} />
                                <span class="control-label"></span>
                            </label>
                        </div>
                    </div>

                    <div class="envision-sidebar-card">
                        <div class="card-badge secondary-bg">
                            <span class="icon-autorenew"></span>
                        </div>
                        <div class="card-text">
                            <h5>Auto Protect <span>(OFF)</span></h5>
                            <label for="switch4" class="switch">
                                <input type="checkbox" name="switch4" id="switch4" onChange={(e)=>{this.switchOn(e)}} />
                                <span class="control-label"></span>
                            </label>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}
