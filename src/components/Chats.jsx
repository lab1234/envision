import React from 'react';
import io from 'socket.io-client';
import avatar from '../img/profile.jpg';
import Axios from 'axios';
const ENDPOINT = 'http://127.0.0.1:5000';


class Chat extends React.Component{
    
    constructor(){
        super();
        this.state = {
            all_messages: [],
            socket : io(ENDPOINT),
            user_data: {}
        }

        this.sendMessage = this.sendMessage.bind(this);
    }

    scroll(){
        let msg_wrapper = document.querySelector('#my-teams');
        
        if(msg_wrapper){
            msg_wrapper.scrollTop = msg_wrapper.scrollHeight;
        }

    }

    componentWillMount(){
        
        this.setState({user_data: this.props.user_data});

        this.state.socket.emit("USER_JOIN", this.props.user_data.username);
        // console.log(this.props.user_data.username)
        // this.state.socket.on("TEST", (e)=>{
        //     console.log(e)
        // });
        this.state.socket.on("SERVER_MESSAGE", msg=>{
            let all_messages = this.state.all_messages;
            // if(msg.receiver===this.props.user_data.username || msg.sender==="You"){
                all_messages.push(msg);
                this.setState({all_messages});
                console.log(msg);

                const data = {
                    'username': this.state.user_data.username,
                    'receiver': (window.location.href).split('chat/')[1]
                }

                Axios.post("/msg-status-change", data).then(res=>{
                    // this.setState({all_messages: res.data});
                    console.log(res.data)
                });

                this.scroll();
                // msg_wrapper.scrollTop = msg_wrapper.scrollHeight;
            // }
        });

    }

    componentDidMount(){
        // Fetch All my messages in chats
        const data = {
            'username': this.state.user_data.username,
            'receiver': (window.location.href).split('chat/')[1]
        }
        Axios.post("/get-messages", data).then(res=>{
            this.setState({all_messages: res.data});
            console.log(res.data)
        });

        Axios.post("/msg-status-change", data).then(res=>{
            // this.setState({all_messages: res.data});
            console.log(res.data)
        });
        this.state.socket.on("TEST", (e)=>{
            this.scroll();
        });
    }
    
    sendMessage(e){
        e.preventDefault();
        const RECEIVER = window.location.href.split("chat/")[1];
        let message = document.querySelector("#message");
        const msg_data = {content: message.value, sender: this.state.user_data.username, date: Date.now(), receiver: RECEIVER}

        if((message.value).trim("").length > 0){
            this.state.socket.emit("CLIENT_MESSAGE", msg_data);
            Axios.post("/send-message", msg_data).then(res=>{
                console.log(res.data)
            })
        }
                
        message.value="";
    }

    render(){
        return(
            <>
            <div className="my-teams" id="my-teams">
                {this.state.all_messages.map((message,index)=>{
                    return(
                            (message.sender==="You" || message.sender===this.props.user_data.username) 
                            ? 
                            (
                                <div key={index} className="each-msg-right">
                                    <h6>You Said: </h6>
                                    <span><img src={avatar} alt="" width="100%"/></span>
                                    <h5>{message.content} <span><span className="icon-timer"></span> {message.date}</span></h5>
                                </div>
                            )
                            :
                                <div key={index} className="each-msg-left">
                                    <h6>{message.sender} Said: </h6>
                                    <span><img src={avatar} alt="" width="100%"/></span>
                                    <h5>{message.content} <span><span className="icon-timer"></span> {message.date}</span></h5>

                                    {/* {console.log(this.props.user_data)} */}
                                </div>
                    )
                })}
                    
            </div>
            <form action="POST" onSubmit={this.sendMessage}>
                <div className="formGroup message-container">
                    <input type="text" id="message" placeholder="Message goes here." autoComplete="off" />
                </div>
            </form>
            
        </>
        )
    }
}

export default Chat;