import React, { Component } from 'react';
import io from 'socket.io-client';
const ENDPOINT = 'http://127.0.0.1:8080';


export default class Main extends Component {
    constructor(){
        super();
        this.state = {
            socket : io(ENDPOINT),
        }
    }

    componentDidMount(){
        this.state.socket.on('stream', stream=>{
            // console.log(stream + '<br/>')
            let video = document.querySelector('#video-feed');
            if(stream){
                video.src = `data:image/jpeg;base64, ${stream}`;
            }
        })
    }
    ShowRightBar = async()=>{
        let overlay = await document.querySelector(".overlay-1");
        overlay.classList.toggle("toggle-overlay");
        let rightbar = await document.querySelector(".right-sidebar");
        rightbar.classList.toggle("showbar");

    }
    render() {
        return (
            <div>
                <div class="app-name-wrapper">
                    <h5 class="app-name">EnVision<span class="app-status"></span></h5>
                    <h6>Environment Supervisory Panel</h6>

                    <h5 onClick={this.ShowRightBar} className="menu-icon icon-wunderlist"><span></span></h5>

                    <div class="input-group">
                        <input type="text" class="input-control" />
                        <span class="placeholder"><span className="icon-security-home"></span> Events</span>
                    </div>
                </div>

                <div class="loader-wrapper">
                    <img id="video-feed" width="100%" height="100%" src alt="feed"/>
                    <div class="stream-decoration">
                        <div class="hr-decoration-left"></div>
                        <div class="hr-decoration-right"></div>
                        <div class="vr-decoration-top"></div>
                        <div class="vr-decoration-bottom"></div>
                    </div>
                    <span class="live-indicator"><span className="icon-video"></span> LIVE</span>
                    <svg class="spinner center-block" width="32px" height="32px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                        <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                    </svg>
                    <h6 class="loader-text">Fetching Video feed ...</h6>

                    <div class="stream-details">
                        <h6>Zone 1</h6>
                        <h6>Video Camera Feed</h6>
                        <p> <span className="icon-calendar"></span> Fri, Nov 13 2020 <span>&copy</span> </p>
                    </div>
                </div>
            </div>
        )
    }
}
