import React from 'react';

import LeftSidebar from './components/LeftSidebar';
import Main from './components/Main';
import RightSidebar from './components/RightSidebar';
import './Loader.css';
import './App.css';



function App() {    
    return (
      <div className="App __layout">
        <LeftSidebar />
        <Main />
        <RightSidebar />
      </div>
    );
  };

export default App;
